CMAKE_MINIMUM_REQUIRED(VERSION 3.0)

INCLUDE(ExternalProject)

ExternalProject_Add(ComponentDatabaseTrjExternal
	PREFIX ComponentDatabaseTrj
	SOURCE_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentDatabaseTrj/smartsoft"
	BINARY_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentDatabaseTrj/smartsoft/build"
	INSTALL_COMMAND ""
)

ExternalProject_Add(ComponentTrajectoryPlannerExternal
	PREFIX ComponentTrajectoryPlanner
	SOURCE_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentTrajectoryPlanner/smartsoft"
	BINARY_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentTrajectoryPlanner/smartsoft/build"
	INSTALL_COMMAND ""
)

ExternalProject_Add(ComponentTrajectoryPlannerBenchmarkExternal
	PREFIX ComponentTrajectoryPlannerBenchmark
	SOURCE_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentTrajectoryPlannerBenchmark/smartsoft"
	BINARY_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentTrajectoryPlannerBenchmark/smartsoft/build"
	INSTALL_COMMAND ""
)

