//--------------------------------------------------------------------------
// Code generated by the SmartSoft MDSD Toolchain
// The SmartSoft Toolchain has been developed by:
//  
// Service Robotics Research Center
// University of Applied Sciences Ulm
// Prittwitzstr. 10
// 89075 Ulm (Germany)
//
// Information about the SmartSoft MDSD Toolchain is available at:
// www.servicerobotik-ulm.de
//
// Please do not modify this file. It will be re-generated
// running the code generator.
//--------------------------------------------------------------------------
#ifndef _ACTIVITY1_CORE_HH
#define _ACTIVITY1_CORE_HH
	
#include "aceSmartSoft.hh"

// include upcall interface
#include "LaserServiceInUpcallInterface.hh"
#include "PoseServiceInUpcallInterface.hh"
#include "TrajectoryServiceInUpcallInterface.hh"

// include communication-objects for output ports
#include <CommBasicObjects/CommNavigationVelocity.hh>

// include all interaction-observer interfaces
#include <Activity1ObserverInterface.hh>


class Activity1Core
:	public SmartACE::ManagedTask
,	public Smart::TaskTriggerSubject
,	public LaserServiceInUpcallInterface
,	public PoseServiceInUpcallInterface
,	public TrajectoryServiceInUpcallInterface
{
private:
	bool useDefaultState; 
	bool useLogging;
	int taskLoggingId;
	unsigned int currentUpdateCount;
	
	Smart::StatusCode laserServiceInStatus;
	CommBasicObjects::CommMobileLaserScan laserServiceInObject;
	Smart::StatusCode poseServiceInStatus;
	CommTrajectory::Pose2D poseServiceInObject;
	Smart::StatusCode trajectoryServiceInStatus;
	CommTrajectory::Trajectory trajectoryServiceInObject;
	
	
protected:
	virtual int execute_protected_region();
	
	virtual void updateAllCommObjects();
	
	virtual int getPreviousCommObjId();
	
	void triggerLogEntry(const int& idOffset);
	
	
	// overload and implement this method in derived classes to immediately get all incoming updates from LaserServiceIn (as soon as they arrive)
	virtual void on_LaserServiceIn(const CommBasicObjects::CommMobileLaserScan &input) {
		// no-op
	}
	
	// this method can be safely used from the thread in derived classes
	inline Smart::StatusCode laserServiceInGetUpdate(CommBasicObjects::CommMobileLaserScan &laserServiceInObject) const
	{
		// copy local object buffer and return the last status code
		laserServiceInObject = this->laserServiceInObject;
		return laserServiceInStatus;
	}
	// overload and implement this method in derived classes to immediately get all incoming updates from PoseServiceIn (as soon as they arrive)
	virtual void on_PoseServiceIn(const CommTrajectory::Pose2D &input) {
		// no-op
	}
	
	// this method can be safely used from the thread in derived classes
	inline Smart::StatusCode poseServiceInGetUpdate(CommTrajectory::Pose2D &poseServiceInObject) const
	{
		// copy local object buffer and return the last status code
		poseServiceInObject = this->poseServiceInObject;
		return poseServiceInStatus;
	}
	// overload and implement this method in derived classes to immediately get all incoming updates from TrajectoryServiceIn (as soon as they arrive)
	virtual void on_TrajectoryServiceIn(const CommTrajectory::Trajectory &input) {
		// no-op
	}
	
	// this method can be safely used from the thread in derived classes
	inline Smart::StatusCode trajectoryServiceInGetUpdate(CommTrajectory::Trajectory &trajectoryServiceInObject) const
	{
		// copy local object buffer and return the last status code
		trajectoryServiceInObject = this->trajectoryServiceInObject;
		return trajectoryServiceInStatus;
	}
	
	// this method is meant to be used in derived classes
	Smart::StatusCode navigationVelocityServiceOutPut(CommBasicObjects::CommNavigationVelocity &navigationVelocityServiceOutDataObject);
	
	
/**
 * Implementation of the Subject part of an InteractionObserver
 */
private:
	std::mutex interaction_observers_mutex;
	std::list<Activity1ObserverInterface*> interaction_observers;
protected:
	void notify_all_interaction_observers();
public:
	void attach_interaction_observer(Activity1ObserverInterface *observer);
	void detach_interaction_observer(Activity1ObserverInterface *observer);

public:
	Activity1Core(Smart::IComponent *comp, const bool &useDefaultState=true);
	virtual ~Activity1Core();
	
	inline void setUpLogging(const int &taskNbr, const bool &useLogging=true) {
		this->taskLoggingId = taskNbr;
		this->useLogging = useLogging;
	}
	
	inline bool isLoggingActive() const {
		return useLogging;
	}
	
	inline int getLoggingID() const {
		return taskLoggingId;
	}
	
	inline int getCurrentUpdateCount() const {
		return currentUpdateCount;
	}
	
};
#endif
