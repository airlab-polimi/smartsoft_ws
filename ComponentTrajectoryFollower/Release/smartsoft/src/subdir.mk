################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../smartsoft/src/Activity1.cc \
../smartsoft/src/CompHandler.cc \
../smartsoft/src/ComponentTrajectoryFollowerCore.cc \
../smartsoft/src/ParameterStateStruct.cc \
../smartsoft/src/SmartStateChangeHandler.cc 

CC_DEPS += \
./smartsoft/src/Activity1.d \
./smartsoft/src/CompHandler.d \
./smartsoft/src/ComponentTrajectoryFollowerCore.d \
./smartsoft/src/ParameterStateStruct.d \
./smartsoft/src/SmartStateChangeHandler.d 

OBJS += \
./smartsoft/src/Activity1.o \
./smartsoft/src/CompHandler.o \
./smartsoft/src/ComponentTrajectoryFollowerCore.o \
./smartsoft/src/ParameterStateStruct.o \
./smartsoft/src/SmartStateChangeHandler.o 


# Each subdirectory must supply rules for building sources it contributes
smartsoft/src/%.o: ../smartsoft/src/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/opt/ACE_wrappers" -I"/home/smartsoft/SOFTWARE/smartsoft/include" -I"/home/smartsoft/SOFTWARE/smartsoft/include/SmartSoft_CD_API" -I"/home/smartsoft/SOFTWARE/smartsoft/include/AceSmartSoftKernel" -I"/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentTrajectoryFollower/smartsoft/src" -I"/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentTrajectoryFollower/smartsoft/src-gen" -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


