################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../smartsoft/src-gen/Activity1Core.cc \
../smartsoft/src-gen/ComponentTrajectoryFollower.cc \
../smartsoft/src-gen/ComponentTrajectoryFollowerAcePortFactory.cc \
../smartsoft/src-gen/ComponentTrajectoryFollowerExtension.cc \
../smartsoft/src-gen/ComponentTrajectoryFollowerImpl.cc \
../smartsoft/src-gen/LaserServiceInUpcallManager.cc \
../smartsoft/src-gen/PoseServiceInUpcallManager.cc \
../smartsoft/src-gen/TrajectoryServiceInUpcallManager.cc \
../smartsoft/src-gen/main.cc 

CC_DEPS += \
./smartsoft/src-gen/Activity1Core.d \
./smartsoft/src-gen/ComponentTrajectoryFollower.d \
./smartsoft/src-gen/ComponentTrajectoryFollowerAcePortFactory.d \
./smartsoft/src-gen/ComponentTrajectoryFollowerExtension.d \
./smartsoft/src-gen/ComponentTrajectoryFollowerImpl.d \
./smartsoft/src-gen/LaserServiceInUpcallManager.d \
./smartsoft/src-gen/PoseServiceInUpcallManager.d \
./smartsoft/src-gen/TrajectoryServiceInUpcallManager.d \
./smartsoft/src-gen/main.d 

OBJS += \
./smartsoft/src-gen/Activity1Core.o \
./smartsoft/src-gen/ComponentTrajectoryFollower.o \
./smartsoft/src-gen/ComponentTrajectoryFollowerAcePortFactory.o \
./smartsoft/src-gen/ComponentTrajectoryFollowerExtension.o \
./smartsoft/src-gen/ComponentTrajectoryFollowerImpl.o \
./smartsoft/src-gen/LaserServiceInUpcallManager.o \
./smartsoft/src-gen/PoseServiceInUpcallManager.o \
./smartsoft/src-gen/TrajectoryServiceInUpcallManager.o \
./smartsoft/src-gen/main.o 


# Each subdirectory must supply rules for building sources it contributes
smartsoft/src-gen/%.o: ../smartsoft/src-gen/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/opt/ACE_wrappers" -I"/home/smartsoft/SOFTWARE/smartsoft/include" -I"/home/smartsoft/SOFTWARE/smartsoft/include/SmartSoft_CD_API" -I"/home/smartsoft/SOFTWARE/smartsoft/include/AceSmartSoftKernel" -I"/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentTrajectoryFollower/smartsoft/src" -I"/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentTrajectoryFollower/smartsoft/src-gen" -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


