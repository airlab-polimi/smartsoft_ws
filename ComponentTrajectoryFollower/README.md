<!--- This file is generated from the ComponentTrajectoryFollower.componentDocumentation model --->
<!--- do not modify this file manually as it will by automatically overwritten by the code generator, modify the model instead and re-generate this file --->

# ComponentTrajectoryFollower Component

![ComponentTrajectoryFollower-ComponentImage](https://github.com/Servicerobotics-Ulm/ComponentRepository/blob/master/ComponentTrajectoryFollower/model/ComponentTrajectoryFollowerComponentDefinition.jpg)


| Metaelement | Documentation |
|-------------|---------------|
| License |  |
| Hardware Requirements |  |
| Purpose |  |



## Service Ports


## Component Parameters ComponentTrajectoryFollowerParams

### ParameterSetInstance Trajectory

