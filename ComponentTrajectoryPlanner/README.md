<!--- This file is generated from the ComponentTrajectoryPlanner.componentDocumentation model --->
<!--- do not modify this file manually as it will by automatically overwritten by the code generator, modify the model instead and re-generate this file --->

# ComponentTrajectoryPlanner Component

![ComponentTrajectoryPlanner-ComponentImage](https://github.com/Servicerobotics-Ulm/ComponentRepository/blob/master/ComponentTrajectoryPlanner/model/ComponentTrajectoryPlannerComponentDefinition.jpg)


| Metaelement | Documentation |
|-------------|---------------|
| License |  |
| Hardware Requirements |  |
| Purpose |  |



## Service Ports


## Component Parameters ComponentTrajectoryPlannerParams

### ParameterSetInstance Trajectory

