//--------------------------------------------------------------------------
// Code generated by the SmartSoft MDSD Toolchain
// The SmartSoft Toolchain has been developed by:
//  
// Service Robotics Research Center
// University of Applied Sciences Ulm
// Prittwitzstr. 10
// 89075 Ulm (Germany)
//
// Information about the SmartSoft MDSD Toolchain is available at:
// www.servicerobotik-ulm.de
//
// Please do not modify this file. It will be re-generated
// running the code generator.
//--------------------------------------------------------------------------
#include "PlanAnswHandlerCore.hh"
#include "PlanAnswHandler.hh"

// include observers

PlanAnswHandlerCore::PlanAnswHandlerCore(Smart::IQueryServerPattern<CommTrajectoryPlanner::PlannerRequest, CommTrajectory::Trajectory, SmartACE::QueryId>* server)
:	Smart::IQueryServerHandler<CommTrajectoryPlanner::PlannerRequest, CommTrajectory::Trajectory, SmartACE::QueryId>(server)
{
	
}

PlanAnswHandlerCore::~PlanAnswHandlerCore()
{
	
}

void PlanAnswHandlerCore::updateAllCommObjects()
{
}

void PlanAnswHandlerCore::notify_all_interaction_observers() {
	std::unique_lock<std::mutex> lock(interaction_observers_mutex);
	// try dynamically down-casting this class to the derived class 
	// (we can do it safely here as we exactly know the derived class)
	if(const PlanAnswHandler* planAnswHandler = dynamic_cast<const PlanAnswHandler*>(this)) {
		for(auto it=interaction_observers.begin(); it!=interaction_observers.end(); it++) {
			(*it)->on_update_from(planAnswHandler);
		}
	}
}

void PlanAnswHandlerCore::attach_interaction_observer(PlanAnswHandlerObserverInterface *observer) {
	std::unique_lock<std::mutex> lock(interaction_observers_mutex);
	interaction_observers.push_back(observer);
}

void PlanAnswHandlerCore::detach_interaction_observer(PlanAnswHandlerObserverInterface *observer) {
	std::unique_lock<std::mutex> lock(interaction_observers_mutex);
	interaction_observers.remove(observer);
}
