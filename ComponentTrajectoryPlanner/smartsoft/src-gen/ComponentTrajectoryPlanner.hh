//--------------------------------------------------------------------------
// Code generated by the SmartSoft MDSD Toolchain
// The SmartSoft Toolchain has been developed by:
//  
// Service Robotics Research Center
// University of Applied Sciences Ulm
// Prittwitzstr. 10
// 89075 Ulm (Germany)
//
// Information about the SmartSoft MDSD Toolchain is available at:
// www.servicerobotik-ulm.de
//
// Please do not modify this file. It will be re-generated
// running the code generator.
//--------------------------------------------------------------------------
#ifndef _COMPONENTTRAJECTORYPLANNER_HH
#define _COMPONENTTRAJECTORYPLANNER_HH

#include <map>
#include <iostream>
#include "aceSmartSoft.hh"
#include "smartQueryServerTaskTrigger_T.h"
#include "ComponentTrajectoryPlannerCore.hh"

#include "ComponentTrajectoryPlannerPortFactoryInterface.hh"
#include "ComponentTrajectoryPlannerExtension.hh"

// forward declarations
class ComponentTrajectoryPlannerPortFactoryInterface;
class ComponentTrajectoryPlannerExtension;

// includes for PlainOpcUaComponentTrajectoryPlannerExtension
// include plain OPC UA device clients
// include plain OPC UA status servers


// include communication objects
#include <CommTrajectoryPlanner/PlannerRequest.hh>
#include <CommTrajectoryPlanner/PlannerRequestACE.hh>
#include <CommTrajectory/Trajectory.hh>
#include <CommTrajectory/TrajectoryACE.hh>

// include tasks
// include UpcallManagers

// include input-handler
// include input-handler
#include "PlanAnswHandler.hh"

// include handler
#include "CompHandler.hh"

#include "ParameterStateStruct.hh"
#include "ParameterUpdateHandler.hh"

#include "SmartStateChangeHandler.hh"

#define COMP ComponentTrajectoryPlanner::instance()

class ComponentTrajectoryPlanner : public ComponentTrajectoryPlannerCore {
private:
	static ComponentTrajectoryPlanner *_componentTrajectoryPlanner;
	
	// constructor
	ComponentTrajectoryPlanner();
	
	// copy-constructor
	ComponentTrajectoryPlanner(const ComponentTrajectoryPlanner& cc);
	
	// destructor
	~ComponentTrajectoryPlanner() { };
	
	// load parameter from ini file
	void loadParameter(int argc, char* argv[]);
	
	// instantiate comp-handler
	CompHandler compHandler;
	
	// helper method that maps a string-name to an according TaskTriggerSubject
	Smart::TaskTriggerSubject* getInputTaskTriggerFromString(const std::string &client);
	
	// internal map storing the different port-creation factories (that internally map to specific middleware implementations)
	std::map<std::string, ComponentTrajectoryPlannerPortFactoryInterface*> portFactoryRegistry;
	
	// internal map storing various extensions of this component class
	std::map<std::string, ComponentTrajectoryPlannerExtension*> componentExtensionRegistry;
	
public:
	ParameterStateStruct getGlobalState() const
	{
		return paramHandler.getGlobalState();
	}
	
	// define tasks
	
	// define input-ports
	
	// define request-ports
	
	// define input-handler
	
	// define output-ports
	
	// define answer-ports
	Smart::IQueryServerPattern<CommTrajectoryPlanner::PlannerRequest, CommTrajectory::Trajectory,SmartACE::QueryId> *planAnsw;
	Smart::QueryServerTaskTrigger<CommTrajectoryPlanner::PlannerRequest, CommTrajectory::Trajectory,SmartACE::QueryId> *planAnswInputTaskTrigger;
	
	// define request-handlers
	PlanAnswHandler *planAnswHandler;
	
	// definitions of PlainOpcUaComponentTrajectoryPlannerExtension
	
	
	// define default slave ports
	SmartACE::StateSlave *stateSlave;
	SmartStateChangeHandler *stateChangeHandler;
	SmartACE::WiringSlave *wiringSlave;
	ParamUpdateHandler paramHandler;
	SmartACE::ParameterSlave *param;
	
	
	/// this method is used to register different PortFactory classes (one for each supported middleware framework)
	void addPortFactory(const std::string &name, ComponentTrajectoryPlannerPortFactoryInterface *portFactory);
	
	/// this method is used to register different component-extension classes
	void addExtension(ComponentTrajectoryPlannerExtension *extension);
	
	/// this method allows to access the registered component-extensions (automatically converting to the actuall implementation type)
	template <typename T>
	T* getExtension(const std::string &name) {
		auto it = componentExtensionRegistry.find(name);
		if(it != componentExtensionRegistry.end()) {
			return dynamic_cast<T*>(it->second);
		}
		return 0;
	}
	
	/// initialize component's internal members
	void init(int argc, char *argv[]);
	
	/// execute the component's infrastructure
	void run();
	
	/// clean-up component's resources
	void fini();
	
	/// call this method to set the overall component into the Alive state (i.e. component is then ready to operate)
	void setStartupFinished();
	
	/// connect all component's client ports
	Smart::StatusCode connectAndStartAllServices();
	
	/// start all assocuated Activities
	void startAllTasks();
	
	/// start all associated timers
	void startAllTimers();
	

	// return singleton instance
	static ComponentTrajectoryPlanner* instance()
	{
		if(_componentTrajectoryPlanner == 0) {
			_componentTrajectoryPlanner = new ComponentTrajectoryPlanner();
		}
		return _componentTrajectoryPlanner;
	}
	
	static void deleteInstance() {
		if(_componentTrajectoryPlanner != 0) {
			delete _componentTrajectoryPlanner;
		}
	}
	
	// connections parameter
	struct connections_struct
	{
		// component struct
		struct component_struct
		{
			// the name of the component
			std::string name;
			std::string initialComponentMode;
			std::string defaultScheduler;
			bool useLogger;
		} component;
		
		//--- task parameter ---
		
		//--- upcall parameter ---
		
		//--- server port parameter ---
		struct PlanAnsw_struct {
				std::string serviceName;
				std::string roboticMiddleware;
		} planAnsw;
	
		//--- client port parameter ---
		
		// -- parameters for PlainOpcUaComponentTrajectoryPlannerExtension
		
	} connections;
};
#endif
