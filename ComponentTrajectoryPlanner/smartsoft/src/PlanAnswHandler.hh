//--------------------------------------------------------------------------
// Code generated by the SmartSoft MDSD Toolchain
// The SmartSoft Toolchain has been developed by:
//  
// Service Robotics Research Center
// University of Applied Sciences Ulm
// Prittwitzstr. 10
// 89075 Ulm (Germany)
//
// Information about the SmartSoft MDSD Toolchain is available at:
// www.servicerobotik-ulm.de
//
// This file is generated once. Modify this file to your needs. 
// If you want the toolchain to re-generate this file, please 
// delete it before running the code generator.
//--------------------------------------------------------------------------
#ifndef _PLANANSWHANDLER_USER_HH
#define _PLANANSWHANDLER_USER_HH
		
#include "PlanAnswHandlerCore.hh"

#include "opencv2/core/core.hpp"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#define FACTOR 50
#define SHOW true

class PlanAnswHandler : public PlanAnswHandlerCore
{
protected:
	char sim_window[4] = "Sim";
	cv::Mat sim_image;
public:
	PlanAnswHandler(Smart::IQueryServerPattern<CommTrajectoryPlanner::PlannerRequest, CommTrajectory::Trajectory, SmartACE::QueryId>* server);
	virtual ~PlanAnswHandler();
	virtual void handleQuery(const SmartACE::QueryId &id, const CommTrajectoryPlanner::PlannerRequest& request);
};
#endif
