<!--- This file is generated from the ComponentTrjFollowerBenchmark.componentDocumentation model --->
<!--- do not modify this file manually as it will by automatically overwritten by the code generator, modify the model instead and re-generate this file --->

# ComponentTrjFollowerBenchmark Component

![ComponentTrjFollowerBenchmark-ComponentImage](https://github.com/Servicerobotics-Ulm/ComponentRepository/blob/master/ComponentTrjFollowerBenchmark/model/ComponentTrjFollowerBenchmarkComponentDefinition.jpg)


| Metaelement | Documentation |
|-------------|---------------|
| License |  |
| Hardware Requirements |  |
| Purpose |  |



## Service Ports


