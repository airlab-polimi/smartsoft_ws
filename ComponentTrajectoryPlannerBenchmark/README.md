<!--- This file is generated from the ComponentTrajectoryPlannerBenchmark.componentDocumentation model --->
<!--- do not modify this file manually as it will by automatically overwritten by the code generator, modify the model instead and re-generate this file --->

# ComponentTrajectoryPlannerBenchmark Component

![ComponentTrajectoryPlannerBenchmark-ComponentImage](https://github.com/Servicerobotics-Ulm/ComponentRepository/blob/master/ComponentTrajectoryPlannerBenchmark/model/ComponentTrajectoryPlannerBenchmarkComponentDefinition.jpg)


| Metaelement | Documentation |
|-------------|---------------|
| License |  |
| Hardware Requirements |  |
| Purpose |  |



## Service Ports


