//--------------------------------------------------------------------------
// Code generated by the SmartSoft MDSD Toolchain
// The SmartSoft Toolchain has been developed by:
//  
// Service Robotics Research Center
// University of Applied Sciences Ulm
// Prittwitzstr. 10
// 89075 Ulm (Germany)
//
// Information about the SmartSoft MDSD Toolchain is available at:
// www.servicerobotik-ulm.de
//
// This file is generated once. Modify this file to your needs. 
// If you want the toolchain to re-generate this file, please 
// delete it before running the code generator.
//--------------------------------------------------------------------------
#ifndef COMMTRAJECTORY_MAP2D_H_
#define COMMTRAJECTORY_MAP2D_H_

#include "CommTrajectory/Map2DCore.hh"

namespace CommTrajectory {
		
class Map2D : public Map2DCore {
	public:
		// default constructors
		Map2D();
		
		/**
		 * Constructor to set all values.
		 * NOTE that you have to keep this constructor consistent with the model!
		 * Use  at your own choice.
		 *
		 * The preferred way to set values for initialization is:
		 *      CommRepository::MyCommObject obj;
		 *      obj.setX(1).setY(2).setZ(3)...;
		 */
		// Map2D(const double &resolution, const unsigned char &occupiedThresh, const unsigned char &freeThresh, const unsigned short &cellSizeX, const unsigned short &cellSizeY, const std::vector<unsigned char> &cells, const std::vector<double> &origin = std::vector<double>(2, 0.0));
		
		Map2D(const Map2DCore &map2D);
		Map2D(const DATATYPE &map2D);
		virtual ~Map2D();
		
		// use framework specific getter and setter methods from core (base) class
		using Map2DCore::get;
		using Map2DCore::set;
		
		//
		// feel free to add customized methods here
		//
};

inline std::ostream &operator<<(std::ostream &os, const Map2D &co)
{
	co.to_ostream(os);
	return os;
}
	
} /* namespace CommTrajectory */
#endif /* COMMTRAJECTORY_MAP2D_H_ */
