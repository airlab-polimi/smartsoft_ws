//--------------------------------------------------------------------------
// Code generated by the SmartSoft MDSD Toolchain
// The SmartSoft Toolchain has been developed by:
//  
// Service Robotics Research Center
// University of Applied Sciences Ulm
// Prittwitzstr. 10
// 89075 Ulm (Germany)
//
// Information about the SmartSoft MDSD Toolchain is available at:
// www.servicerobotik-ulm.de
//
// Please do not modify this file. It will be re-generated
// running the code generator.
//--------------------------------------------------------------------------
#include "CommTrajectory/TrajectoryCore.hh"

// serialization/deserialization operators
//#include "CommTrajectory/TrajectoryACE.hh"

// include the hash.idl containing the hash constant
#include "hash.hh"
#include <assert.h>
#include <cstring>
#include <iostream>

// SmartUtils used in from_xml method
#include "smartKnuthMorrisPratt.hh"

#ifdef ENABLE_HASH
	#include <boost/functional/hash.hpp>
#endif

namespace CommTrajectory 
{
	const char* TrajectoryCore::getCompiledHash()
	{
		return CommTrajectoryIDL::REPO_HASH;
	}
	
	void TrajectoryCore::getAllHashValues(std::list<std::string> &hashes)
	{
		// get own hash value
		hashes.push_back(getCompiledHash());
		// get hash value(s) for CommTrajectory::Pose2D(idl_Trajectory.trajectory)
		CommTrajectory::Pose2D::getAllHashValues(hashes);
	}
	
	void TrajectoryCore::checkAllHashValues(std::list<std::string> &hashes)
	{
		// check own hash value
		if (strcmp(getCompiledHash(), hashes.front().c_str()) != 0)
		{
			std::cerr << "###################################################" << std::endl;
			std::cerr << "WARNING: HASHES OF COMMUNICATION OBJECTS MISSMATCH!" << std::endl;
			std::cerr << "TrajectoryCore hash" << std::endl;
			std::cerr << "Expected: " << getCompiledHash() << std::endl;
			std::cerr << "Received: " << hashes.front() << std::endl;
			std::cerr << "###################################################" << std::endl;
		}
		assert(strcmp(getCompiledHash(), hashes.front().c_str()) == 0);
		hashes.pop_front();
		
		// check hash value(s) for CommTrajectory::Pose2D(idl_Trajectory.trajectory)
		CommTrajectory::Pose2D::checkAllHashValues(hashes);
	}
	
	#ifdef ENABLE_HASH
	size_t TrajectoryCore::generateDataHash(const DATATYPE &data)
	{
		size_t seed = 0;
		
		std::vector<CommTrajectoryIDL::Pose2D>::const_iterator data_trajectoryIt;
		for(data_trajectoryIt=data.trajectory.begin(); data_trajectoryIt!=data.trajectory.end(); data_trajectoryIt++) {
			seed += CommTrajectory::Pose2D::generateDataHash(*data_trajectoryIt);
		}
		
		return seed;
	}
	#endif
	
	// default constructor
	TrajectoryCore::TrajectoryCore()
	:	idl_Trajectory()
	{  
		setTrajectory(std::vector<CommTrajectory::Pose2D>());
	}
	
	TrajectoryCore::TrajectoryCore(const DATATYPE &data)
	:	idl_Trajectory(data)
	{  }

	TrajectoryCore::~TrajectoryCore()
	{  }
	
	void TrajectoryCore::to_ostream(std::ostream &os) const
	{
	  os << "Trajectory(";
	  std::vector<CommTrajectory::Pose2D>::const_iterator trajectoryIt;
	  std::vector<CommTrajectory::Pose2D> trajectoryList = getTrajectoryCopy();
	  for(trajectoryIt=trajectoryList.begin(); trajectoryIt!=trajectoryList.end(); trajectoryIt++) {
	  	trajectoryIt->to_ostream(os);
	  }
	  os << ") ";
	}
	
	// convert to xml stream
	void TrajectoryCore::to_xml(std::ostream &os, const std::string &indent) const {
		size_t counter = 0;
		
		std::vector<CommTrajectory::Pose2D>::const_iterator trajectoryIt;
		std::vector<CommTrajectory::Pose2D> trajectoryList = getTrajectoryCopy();
		counter = 0;
		os << indent << "<trajectoryList n=\"" << trajectoryList.size() << "\">";
		for(trajectoryIt=trajectoryList.begin(); trajectoryIt!=trajectoryList.end(); trajectoryIt++) {
			os << indent << "<trajectory i=\"" << counter++ << "\">";
			trajectoryIt->to_xml(os, indent);
			os << indent << "</trajectory>";
		}
		os << indent << "</trajectoryList>";
	}
	
	// restore from xml stream
	void TrajectoryCore::from_xml(std::istream &is) {
		size_t counter = 0;
		
		static const Smart::KnuthMorrisPratt kmp_trajectoryList("<trajectoryList n=\"");
		static const Smart::KnuthMorrisPratt kmp_trajectory("\">");
		
		if(kmp_trajectoryList.search(is)) {
			size_t numberElements;
			is >> numberElements;
			CommTrajectory::Pose2D trajectoryItem;
			std::vector<CommTrajectory::Pose2D> trajectoryList;
			kmp_trajectory.search(is);
			for(counter=0; counter<numberElements; counter++) {
				if(kmp_trajectory.search(is)) {
					trajectoryItem.from_xml(is);
					trajectoryList.push_back(trajectoryItem);
				}
			}
			setTrajectory(trajectoryList);
		}
	}
	
	/*
	void TrajectoryCore::get(ACE_Message_Block *&msg) const
	{
		// start with a default internal buffer size(will automatically grow if needed)
		ACE_OutputCDR cdr(ACE_DEFAULT_CDR_BUFSIZE);
		
		CommTrajectoryIDL::HashList hashes;
		getAllHashValues(hashes);
		cdr << static_cast<ACE_CDR::Long>(hashes.size());
		for(CommTrajectoryIDL::HashList::const_iterator it=hashes.begin(); it!=hashes.end(); it++)
		{
			cdr << ACE_CString(it->c_str());
		}
		
		// Here the actual serialization takes place using the OutputCDR serialization operator<<
		// (see TrajectoryACE.hh)
		cdr << idl_Trajectory;
		
	#ifdef ENABLE_HASH
		ACE_CDR::ULong data_hash = generateDataHash(idl_Trajectory);
		cdr << data_hash;
		// std::cout << "TrajectoryCore: current data hash: " << data_hash << std::endl;
	#endif
		
		// return a shallow copy of the serialized message 
		// (no data is actually copied, only the internal reference counter is incremented)
		// in order to prevent memory leaks the caller of this get(msg) method must
		// manually free the memory by calling the release() method of the message block msg
		msg = cdr.begin()->duplicate();
	}
	
	void TrajectoryCore::set(const ACE_Message_Block *msg)
	{
		ACE_InputCDR cdr(msg);
	
		CommTrajectoryIDL::HashList hashes;
		ACE_CDR::Long hashes_size;
		cdr >> hashes_size;
		for(int i=0; i<hashes_size; ++i) 
		{
			ACE_CString hash;
			cdr >> hash;
			hashes.push_back(hash.c_str());
		}
		checkAllHashValues(hashes);
		
		// Here the actual de-serialization takes place using the InputCDR serialization operator>>
		// (see TrajectoryACE.hh)
		cdr >> idl_Trajectory;
		
	#ifdef ENABLE_HASH
		ACE_CDR::Long data_hash;
		cdr >> data_hash;
		ACE_CDR::Long own_hash = generateDataHash(idl_Trajectory);
		assert(data_hash == own_hash);
		// std::cout << "TrajectoryCore: own data hash: " << own_hash << "; received data hash: " << data_hash << std::endl;
	#endif
	}
	*/
} /* namespace CommTrajectory */
