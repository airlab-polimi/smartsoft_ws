//--------------------------------------------------------------------------
// Code generated by the SmartSoft MDSD Toolchain
// The SmartSoft Toolchain has been developed by:
//  
// Service Robotics Research Center
// University of Applied Sciences Ulm
// Prittwitzstr. 10
// 89075 Ulm (Germany)
//
// Information about the SmartSoft MDSD Toolchain is available at:
// www.servicerobotik-ulm.de
//
// Please do not modify this file. It will be re-generated
// running the code generator.
//--------------------------------------------------------------------------
#ifndef COMMTRAJECTORY_MAP2D_CORE_H_
#define COMMTRAJECTORY_MAP2D_CORE_H_

#include "CommTrajectory/Map2DData.hh"
#include "CommTrajectory/Pose2D.hh"

#include <iostream>
#include <string>
#include <list>

namespace CommTrajectory {
	
class Map2DCore {
protected:
	// data structure
	CommTrajectoryIDL::Map2D idl_Map2D;
	
public:
	// give a publicly accessible type-name for the template parameter IDL
	typedef CommTrajectoryIDL::Map2D DATATYPE;
	
	#ifdef ENABLE_HASH
		static size_t generateDataHash(const DATATYPE &);
	#endif
	
	static const char* getCompiledHash();
	static void getAllHashValues(std::list<std::string> &hashes);
	static void checkAllHashValues(std::list<std::string> &hashes);
	
	// default constructors
	Map2DCore();
	Map2DCore(const DATATYPE &data);
	// default destructor
	virtual ~Map2DCore();
	
	const DATATYPE& get() const { return idl_Map2D; }
	operator const DATATYPE&() const { return idl_Map2D; }
	DATATYPE& set() { return idl_Map2D; }

	static inline std::string identifier(void) { return "CommTrajectory::Map2D"; }
	
	// helper method to easily implement output stream in derived classes
	void to_ostream(std::ostream &os = std::cout) const;
	
	// convert to xml stream
	void to_xml(std::ostream &os, const std::string &indent = "") const;
	
	// restore from xml stream
	void from_xml(std::istream &is);
	
	// User Interface
	
	// getter and setter for element Resolution
	inline double getResolution() const { return idl_Map2D.resolution; }
	inline Map2DCore& setResolution(const double &resolution) { idl_Map2D.resolution = resolution; return *this; }
	
	// getter and setter for element Origin
	inline CommTrajectory::Pose2D getOrigin() const { return CommTrajectory::Pose2D(idl_Map2D.origin); }
	inline Map2DCore& setOrigin(const CommTrajectory::Pose2D &origin) { idl_Map2D.origin = origin; return *this; }
	
	// getter and setter for element OccupiedThresh
	inline unsigned char getOccupiedThresh() const { return idl_Map2D.occupiedThresh; }
	inline Map2DCore& setOccupiedThresh(const unsigned char &occupiedThresh) { idl_Map2D.occupiedThresh = occupiedThresh; return *this; }
	
	// getter and setter for element FreeThresh
	inline unsigned char getFreeThresh() const { return idl_Map2D.freeThresh; }
	inline Map2DCore& setFreeThresh(const unsigned char &freeThresh) { idl_Map2D.freeThresh = freeThresh; return *this; }
	
	// getter and setter for element CellSizeX
	inline unsigned short getCellSizeX() const { return idl_Map2D.cellSizeX; }
	inline Map2DCore& setCellSizeX(const unsigned short &cellSizeX) { idl_Map2D.cellSizeX = cellSizeX; return *this; }
	
	// getter and setter for element CellSizeY
	inline unsigned short getCellSizeY() const { return idl_Map2D.cellSizeY; }
	inline Map2DCore& setCellSizeY(const unsigned short &cellSizeY) { idl_Map2D.cellSizeY = cellSizeY; return *this; }
	
	// getter and setter for element Cells
	/**
	 * Getter methods for idl_Map2D.cells of type vector<unsigned char>
	 */
	inline std::vector<unsigned char>& getCellsRef() { return idl_Map2D.cells; }
	inline std::vector<unsigned char> getCellsCopy() const {
		return std::vector<unsigned char>(idl_Map2D.cells.begin(), idl_Map2D.cells.end());
	}
	inline unsigned char getCellsElemAtPos(const size_t &pos) const { return idl_Map2D.cells[pos]; }
	inline size_t getCellsSize() const { return idl_Map2D.cells.size(); }
	inline bool isCellsEmpty() const { return idl_Map2D.cells.empty(); }
	/**
	 * Setter methods for idl_Map2D.cells of type vector<unsigned char>
	 */
	inline Map2DCore& setCells(const std::vector<unsigned char> &cells) { idl_Map2D.cells = cells; return *this; }
	inline bool setCellsElemAtPos(const size_t &pos, const unsigned char &elem) {
		if(pos < idl_Map2D.cells.size()) {
			idl_Map2D.cells[pos] = elem;
			return true;
		}
		return false;
	}
	inline bool insertCellsVectorAtPos(const size_t &pos, const std::vector<unsigned char> &data) {
		if(pos < idl_Map2D.cells.size()) {
			idl_Map2D.cells.insert(idl_Map2D.cells.begin()+pos, data.begin(), data.end());
			return true;
		}
		return false;
	}
	inline void resizeCells(const size_t &size) { idl_Map2D.cells.resize(size); }
	inline bool eraseCellsElemsAtPos(const size_t &pos, const size_t &nbr_elems) {
		if( (pos+nbr_elems) <= idl_Map2D.cells.size() ) {
			idl_Map2D.cells.erase(idl_Map2D.cells.begin()+pos, idl_Map2D.cells.begin()+pos+nbr_elems);
			return true;
		}
		return false;
	}
	inline void clearCells() { idl_Map2D.cells.clear(); }
};

} /* namespace CommTrajectory */
#endif /* COMMTRAJECTORY_MAP2D_CORE_H_ */
