##Trajecotry planner benchmark - how to:
###Requirementes:
- working installation of SmartMDSD
- OpenCV

###Setup:
- clone the following  repository: https://bitbucket.org/airlab-polimi/smartsoft_ws/src/master/
- Import the workspace in the SmartMDSD environment
- Run cmake and build the following components:
    - *CommTrajectory*
    - *CommTrajectoryPlanner*
    - *CommTrajectoryPlannerBenchmark*
    - *ComponentDatabaseTrj*
    - *ComponentTrajectoryPlanner*
    - *ComponentTrajectoryPlannerBenchmark*
- Run cmake and build the following system:
    - *SystemTrajectoryPlannerBenchmark*

###Input
- ***standardized_problem.json*** in the home folder
- ***locations.json*** in the home folder
- map files as pgm images in the locations specified in *locations.json*

###Execution
- Run *SystemTrajectoryPlannerBenchmark*
- Locate the *ComponentTrajectoryPlannerBenchmark* terminal
- Press any button when prompted
- Wait for the benchmark to terminate

###Output
- ***benchmark.log*** in the home folder

###JSON and log examples
Also available in the *resource* folder in the repository

*standardized_problem.json*
```json
{
  "scenario1": {
    "tuple1": {"minClutter": 0.5, "maxClutter": 1},
    "tuple2": {"minClutter": 0.5, "maxClutter": 1, "minPassage": 0.1, "maxPassage": 1.3}
  },
  "scenario2": {
    "tuple1": {"minPassage": 0.1, "maxPassage": 3.2}
  }
}
```

*locations.json*
```json
{
  "list": [
    {
      "starts": [[2.6, 2.6], [2.1, 17.1]],
      "goals": [[17.3, 17.4], [17.9, 2.7]],
      "clutter": 0.1,
      "passage": 2.0,
      "map": {
        "location": "/home/smartsoft/map.pgm",
        "resolution": 1.0,
        "origin": [0.0, 0.0],
        "occupiedThresh": 0.5,
        "freeThresh": 0.0
      }
    },
    {
      "starts": [[16.4, 11.2], [12.7, 13.6]],
      "goals": [[9.1, 10.3], [17.2, 17.9]],
      "clutter": 0.9,
      "passage": 0.5,
      "map": {
        "location": "/home/smartsoft/map.pgm",
        "resolution": 1.0,
        "origin": [0.0, 0.0],
        "occupiedThresh": 0.5,
        "freeThresh": 0.0
      }
    }
  ]
}
```
*benchmark.log*
```
[2019-03-21 14:22:54] Benchmark initialized successfully
[2019-03-21 14:22:54] Using the following standardized problem configuration: 
[2019-03-21 14:22:54] 
{
  "scenario1": {
    "tuple1": {
      "maxClutter": 1,
      "minClutter": 0.5
    },
    "tuple2": {
      "maxClutter": 1,
      "maxPassage": 1.3,
      "minClutter": 0.5,
      "minPassage": 0.1
    }
  },
  "scenario2": {
    "tuple1": {
      "maxPassage": 3.2,
      "minPassage": 0.1
    }
  }
}
[2019-03-21 14:22:54] Executing scenario: scenaio1
[2019-03-21 14:22:54] Executing tuple: tuple1
[2019-03-21 14:22:54] Benchmarking the system with the following configurations: 
[2019-03-21 14:22:54] Configuration: 0
[2019-03-21 14:22:54] Minimum euclidian distance: 7.35527, Planned distance: 22.3906
[2019-03-21 14:22:54] Performance measure on distance: 2.04416
[2019-03-21 14:22:54] Minimum euclidian distance: 6.22415, Planned distance: 30.6689
[2019-03-21 14:22:54] Performance measure on distance: 3.92741
[2019-03-21 14:22:54] Average performance measure of the tuple: 2.98578

[2019-03-21 14:22:54] Executing tuple: tuple2
[2019-03-21 14:22:54] Benchmarking the system with the following configurations: 
[2019-03-21 14:22:54] Configuration: 0
[2019-03-21 14:22:54] Minimum euclidian distance: 7.35527, Planned distance: 22.3906
[2019-03-21 14:22:54] Performance measure on distance: 2.04416
[2019-03-21 14:22:54] Minimum euclidian distance: 6.22415, Planned distance: 30.6689
[2019-03-21 14:22:54] Performance measure on distance: 3.92741
[2019-03-21 14:22:54] Average performance measure of the tuple: 2.98578

[2019-03-21 14:22:54] Executing scenario: scenario2
[2019-03-21 14:22:54] Executing tuple: tuple1
[2019-03-21 14:22:54] Benchmarking the system with the following configurations: 
[2019-03-21 14:22:54] Configuration: 0
[2019-03-21 14:22:54] Minimum euclidian distance: 20.8598, Planned distance: 27.9381
[2019-03-21 14:22:54] Performance measure on distance: 0.33933
[2019-03-21 14:22:54] Minimum euclidian distance: 21.3776, Planned distance: 25.7072
[2019-03-21 14:22:54] Performance measure on distance: 0.20253
[2019-03-21 14:22:54] Configuration: 1
[2019-03-21 14:22:54] Minimum euclidian distance: 7.35527, Planned distance: 22.3906
[2019-03-21 14:22:54] Performance measure on distance: 2.04416
[2019-03-21 14:22:54] Minimum euclidian distance: 6.22415, Planned distance: 30.6689
[2019-03-21 14:22:54] Performance measure on distance: 3.92741
[2019-03-21 14:22:54] Average performance measure of the tuple: 1.62836


```