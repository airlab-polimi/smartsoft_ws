//--------------------------------------------------------------------------
// Code generated by the SmartSoft MDSD Toolchain
// The SmartSoft Toolchain has been developed by:
//  
// Service Robotics Research Center
// University of Applied Sciences Ulm
// Prittwitzstr. 10
// 89075 Ulm (Germany)
//
// Information about the SmartSoft MDSD Toolchain is available at:
// www.servicerobotik-ulm.de
//
// Please do not modify this file. It will be re-generated
// running the code generator.
//--------------------------------------------------------------------------
#ifndef COMMTRAJECTORYPLANNERBENCHMARK_QUERY_ACE_H_
#define COMMTRAJECTORYPLANNERBENCHMARK_QUERY_ACE_H_

#include "CommTrajectoryPlannerBenchmark/Query.hh"

#include <ace/CDR_Stream.h>

// serialization operator for DataStructure Query
ACE_CDR::Boolean operator<<(ACE_OutputCDR &cdr, const CommTrajectoryPlannerBenchmarkIDL::Query &data);

// de-serialization operator for DataStructure Query
ACE_CDR::Boolean operator>>(ACE_InputCDR &cdr, CommTrajectoryPlannerBenchmarkIDL::Query &data);

// serialization operator for CommunicationObject Query
ACE_CDR::Boolean operator<<(ACE_OutputCDR &cdr, const CommTrajectoryPlannerBenchmark::Query &obj);

// de-serialization operator for CommunicationObject Query
ACE_CDR::Boolean operator>>(ACE_InputCDR &cdr, CommTrajectoryPlannerBenchmark::Query &obj);

#endif /* COMMTRAJECTORYPLANNERBENCHMARK_QUERY_ACE_H_ */
