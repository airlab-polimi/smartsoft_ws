//--------------------------------------------------------------------------
// Code generated by the SmartSoft MDSD Toolchain
// The SmartSoft Toolchain has been developed by:
//  
// Service Robotics Research Center
// University of Applied Sciences Ulm
// Prittwitzstr. 10
// 89075 Ulm (Germany)
//
// Information about the SmartSoft MDSD Toolchain is available at:
// www.servicerobotik-ulm.de
//
// Please do not modify this file. It will be re-generated
// running the code generator.
//--------------------------------------------------------------------------
#include "CommTrajectoryFollowerBenchmark/QueryACE.hh"
#include <ace/SString.h>

// serialization operator for element Query
ACE_CDR::Boolean operator<<(ACE_OutputCDR &cdr, const CommTrajectoryFollowerBenchmarkIDL::Query &data)
{
	ACE_CDR::Boolean good_bit = true;
	// serialize list-element minDistanceFromStart
	good_bit = good_bit && cdr.write_double(data.minDistanceFromStart);
	// serialize list-element maxDistanceFromStart
	good_bit = good_bit && cdr.write_double(data.maxDistanceFromStart);
	// serialize list-element minAngleFromStart
	good_bit = good_bit && cdr.write_double(data.minAngleFromStart);
	// serialize list-element maxAngleFromStart
	good_bit = good_bit && cdr.write_double(data.maxAngleFromStart);
	// serialize list-element minCurvature
	good_bit = good_bit && cdr.write_double(data.minCurvature);
	// serialize list-element maxCurvature
	good_bit = good_bit && cdr.write_double(data.maxCurvature);
	// serialize list-element minClutter
	good_bit = good_bit && cdr.write_float(data.minClutter);
	// serialize list-element maxClutter
	good_bit = good_bit && cdr.write_float(data.maxClutter);
	
	return good_bit;
}

// de-serialization operator for element Query
ACE_CDR::Boolean operator>>(ACE_InputCDR &cdr, CommTrajectoryFollowerBenchmarkIDL::Query &data)
{
	ACE_CDR::Boolean good_bit = true;
	// deserialize type element minDistanceFromStart
	good_bit = good_bit && cdr.read_double(data.minDistanceFromStart);
	// deserialize type element maxDistanceFromStart
	good_bit = good_bit && cdr.read_double(data.maxDistanceFromStart);
	// deserialize type element minAngleFromStart
	good_bit = good_bit && cdr.read_double(data.minAngleFromStart);
	// deserialize type element maxAngleFromStart
	good_bit = good_bit && cdr.read_double(data.maxAngleFromStart);
	// deserialize type element minCurvature
	good_bit = good_bit && cdr.read_double(data.minCurvature);
	// deserialize type element maxCurvature
	good_bit = good_bit && cdr.read_double(data.maxCurvature);
	// deserialize type element minClutter
	good_bit = good_bit && cdr.read_float(data.minClutter);
	// deserialize type element maxClutter
	good_bit = good_bit && cdr.read_float(data.maxClutter);
	
	return good_bit;
}

// serialization operator for CommunicationObject Query
ACE_CDR::Boolean operator<<(ACE_OutputCDR &cdr, const CommTrajectoryFollowerBenchmark::Query &obj)
{
	return cdr << obj.get();
}

// de-serialization operator for CommunicationObject Query
ACE_CDR::Boolean operator>>(ACE_InputCDR &cdr, CommTrajectoryFollowerBenchmark::Query &obj)
{
	return cdr >> obj.set();
}
