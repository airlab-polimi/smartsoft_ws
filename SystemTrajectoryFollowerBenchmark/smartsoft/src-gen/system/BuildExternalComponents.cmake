CMAKE_MINIMUM_REQUIRED(VERSION 3.0)

INCLUDE(ExternalProject)

ExternalProject_Add(ComponentRobotSimulatorExternal
	PREFIX ComponentRobotSimulator
	SOURCE_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentRobotSimulator/smartsoft"
	BINARY_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentRobotSimulator/smartsoft/build"
	INSTALL_COMMAND ""
)

ExternalProject_Add(ComponentTrajectoryFollowerExternal
	PREFIX ComponentTrajectoryFollower
	SOURCE_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentTrajectoryFollower/smartsoft"
	BINARY_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentTrajectoryFollower/smartsoft/build"
	INSTALL_COMMAND ""
)

ExternalProject_Add(ComponentTrjFollowerBenchmarkExternal
	PREFIX ComponentTrjFollowerBenchmark
	SOURCE_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentTrjFollowerBenchmark/smartsoft"
	BINARY_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentTrjFollowerBenchmark/smartsoft/build"
	INSTALL_COMMAND ""
)

ExternalProject_Add(ComponentDatabaseFlwExternal
	PREFIX ComponentDatabaseFlw
	SOURCE_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentDatabaseFlw/smartsoft"
	BINARY_DIR "/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentDatabaseFlw/smartsoft/build"
	INSTALL_COMMAND ""
)

