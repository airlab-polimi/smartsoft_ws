<!--- This file is generated from the ComponentRobotSimulator.componentDocumentation model --->
<!--- do not modify this file manually as it will by automatically overwritten by the code generator, modify the model instead and re-generate this file --->

# ComponentRobotSimulator Component

![ComponentRobotSimulator-ComponentImage](https://github.com/Servicerobotics-Ulm/ComponentRepository/blob/master/ComponentRobotSimulator/model/ComponentRobotSimulatorComponentDefinition.jpg)


| Metaelement | Documentation |
|-------------|---------------|
| License |  |
| Hardware Requirements |  |
| Purpose |  |



## Service Ports


## Component Parameters ComponentRobotSimulatorParams

### ParameterSetInstance Trajectory

