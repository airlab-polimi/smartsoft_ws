################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CC_SRCS += \
../smartsoft/src-gen/Activity1Core.cc \
../smartsoft/src-gen/ComponentRobotSimulator.cc \
../smartsoft/src-gen/ComponentRobotSimulatorAcePortFactory.cc \
../smartsoft/src-gen/ComponentRobotSimulatorExtension.cc \
../smartsoft/src-gen/ComponentRobotSimulatorImpl.cc \
../smartsoft/src-gen/MapServiceInUpcallManager.cc \
../smartsoft/src-gen/NavigationVelocityServiceInUpcallManager.cc \
../smartsoft/src-gen/PoseServiceInUpcallManager.cc \
../smartsoft/src-gen/main.cc 

CC_DEPS += \
./smartsoft/src-gen/Activity1Core.d \
./smartsoft/src-gen/ComponentRobotSimulator.d \
./smartsoft/src-gen/ComponentRobotSimulatorAcePortFactory.d \
./smartsoft/src-gen/ComponentRobotSimulatorExtension.d \
./smartsoft/src-gen/ComponentRobotSimulatorImpl.d \
./smartsoft/src-gen/MapServiceInUpcallManager.d \
./smartsoft/src-gen/NavigationVelocityServiceInUpcallManager.d \
./smartsoft/src-gen/PoseServiceInUpcallManager.d \
./smartsoft/src-gen/main.d 

OBJS += \
./smartsoft/src-gen/Activity1Core.o \
./smartsoft/src-gen/ComponentRobotSimulator.o \
./smartsoft/src-gen/ComponentRobotSimulatorAcePortFactory.o \
./smartsoft/src-gen/ComponentRobotSimulatorExtension.o \
./smartsoft/src-gen/ComponentRobotSimulatorImpl.o \
./smartsoft/src-gen/MapServiceInUpcallManager.o \
./smartsoft/src-gen/NavigationVelocityServiceInUpcallManager.o \
./smartsoft/src-gen/PoseServiceInUpcallManager.o \
./smartsoft/src-gen/main.o 


# Each subdirectory must supply rules for building sources it contributes
smartsoft/src-gen/%.o: ../smartsoft/src-gen/%.cc
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -I"/opt/ACE_wrappers" -I"/home/smartsoft/SOFTWARE/smartsoft/include" -I"/home/smartsoft/SOFTWARE/smartsoft/include/SmartSoft_CD_API" -I"/home/smartsoft/SOFTWARE/smartsoft/include/AceSmartSoftKernel" -I"/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentRobotSimulator/smartsoft/src" -I"/home/smartsoft/workspaces/SmartMDSD-Toolchain/ComponentRobotSimulator/smartsoft/src-gen" -O3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


