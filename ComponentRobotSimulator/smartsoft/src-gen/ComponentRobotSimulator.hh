//--------------------------------------------------------------------------
// Code generated by the SmartSoft MDSD Toolchain
// The SmartSoft Toolchain has been developed by:
//  
// Service Robotics Research Center
// University of Applied Sciences Ulm
// Prittwitzstr. 10
// 89075 Ulm (Germany)
//
// Information about the SmartSoft MDSD Toolchain is available at:
// www.servicerobotik-ulm.de
//
// Please do not modify this file. It will be re-generated
// running the code generator.
//--------------------------------------------------------------------------
#ifndef _COMPONENTROBOTSIMULATOR_HH
#define _COMPONENTROBOTSIMULATOR_HH

#include <map>
#include <iostream>
#include "aceSmartSoft.hh"
#include "smartQueryServerTaskTrigger_T.h"
#include "ComponentRobotSimulatorCore.hh"

#include "ComponentRobotSimulatorPortFactoryInterface.hh"
#include "ComponentRobotSimulatorExtension.hh"

// forward declarations
class ComponentRobotSimulatorPortFactoryInterface;
class ComponentRobotSimulatorExtension;

// includes for PlainOpcUaComponentRobotSimulatorExtension
// include plain OPC UA device clients
// include plain OPC UA status servers


// include communication objects
#include <CommBasicObjects/CommMobileLaserScan.hh>
#include <CommBasicObjects/CommMobileLaserScanACE.hh>
#include <CommBasicObjects/CommNavigationVelocity.hh>
#include <CommBasicObjects/CommNavigationVelocityACE.hh>
#include <CommTrajectory/Map2D.hh>
#include <CommTrajectory/Map2DACE.hh>
#include <CommTrajectory/Pose2D.hh>
#include <CommTrajectory/Pose2DACE.hh>

// include tasks
#include "Activity1.hh"
// include UpcallManagers
#include "MapServiceInUpcallManager.hh"
#include "NavigationVelocityServiceInUpcallManager.hh"
#include "PoseServiceInUpcallManager.hh"

// include input-handler
// include input-handler

// include handler
#include "CompHandler.hh"

#include "ParameterStateStruct.hh"
#include "ParameterUpdateHandler.hh"

#include "SmartStateChangeHandler.hh"

#define COMP ComponentRobotSimulator::instance()

class ComponentRobotSimulator : public ComponentRobotSimulatorCore {
private:
	static ComponentRobotSimulator *_componentRobotSimulator;
	
	// constructor
	ComponentRobotSimulator();
	
	// copy-constructor
	ComponentRobotSimulator(const ComponentRobotSimulator& cc);
	
	// destructor
	~ComponentRobotSimulator() { };
	
	// load parameter from ini file
	void loadParameter(int argc, char* argv[]);
	
	// instantiate comp-handler
	CompHandler compHandler;
	
	// helper method that maps a string-name to an according TaskTriggerSubject
	Smart::TaskTriggerSubject* getInputTaskTriggerFromString(const std::string &client);
	
	// internal map storing the different port-creation factories (that internally map to specific middleware implementations)
	std::map<std::string, ComponentRobotSimulatorPortFactoryInterface*> portFactoryRegistry;
	
	// internal map storing various extensions of this component class
	std::map<std::string, ComponentRobotSimulatorExtension*> componentExtensionRegistry;
	
public:
	ParameterStateStruct getGlobalState() const
	{
		return paramHandler.getGlobalState();
	}
	
	// define tasks
	Smart::TaskTriggerSubject* activity1Trigger;
	Activity1 *activity1;
	
	// define input-ports
	// InputPort MapServiceIn
	Smart::ISendServerPattern<CommTrajectory::Map2D> *mapServiceIn;
	Smart::InputTaskTrigger<CommTrajectory::Map2D> *mapServiceInInputTaskTrigger;
	MapServiceInUpcallManager *mapServiceInUpcallManager;
	// InputPort NavigationVelocityServiceIn
	Smart::ISendServerPattern<CommBasicObjects::CommNavigationVelocity> *navigationVelocityServiceIn;
	Smart::InputTaskTrigger<CommBasicObjects::CommNavigationVelocity> *navigationVelocityServiceInInputTaskTrigger;
	NavigationVelocityServiceInUpcallManager *navigationVelocityServiceInUpcallManager;
	// InputPort PoseServiceIn
	Smart::IPushClientPattern<CommTrajectory::Pose2D> *poseServiceIn;
	Smart::InputTaskTrigger<CommTrajectory::Pose2D> *poseServiceInInputTaskTrigger;
	PoseServiceInUpcallManager *poseServiceInUpcallManager;
	
	// define request-ports
	
	// define input-handler
	
	// define output-ports
	Smart::IPushServerPattern<CommBasicObjects::CommMobileLaserScan> *laserServiceOut;
	Smart::IPushServerPattern<CommTrajectory::Pose2D> *poseServiceOut;
	
	// define answer-ports
	
	// define request-handlers
	
	// definitions of PlainOpcUaComponentRobotSimulatorExtension
	
	
	// define default slave ports
	SmartACE::StateSlave *stateSlave;
	SmartStateChangeHandler *stateChangeHandler;
	SmartACE::WiringSlave *wiringSlave;
	ParamUpdateHandler paramHandler;
	SmartACE::ParameterSlave *param;
	
	
	/// this method is used to register different PortFactory classes (one for each supported middleware framework)
	void addPortFactory(const std::string &name, ComponentRobotSimulatorPortFactoryInterface *portFactory);
	
	/// this method is used to register different component-extension classes
	void addExtension(ComponentRobotSimulatorExtension *extension);
	
	/// this method allows to access the registered component-extensions (automatically converting to the actuall implementation type)
	template <typename T>
	T* getExtension(const std::string &name) {
		auto it = componentExtensionRegistry.find(name);
		if(it != componentExtensionRegistry.end()) {
			return dynamic_cast<T*>(it->second);
		}
		return 0;
	}
	
	/// initialize component's internal members
	void init(int argc, char *argv[]);
	
	/// execute the component's infrastructure
	void run();
	
	/// clean-up component's resources
	void fini();
	
	/// call this method to set the overall component into the Alive state (i.e. component is then ready to operate)
	void setStartupFinished();
	
	/// connect all component's client ports
	Smart::StatusCode connectAndStartAllServices();
	
	/// start all assocuated Activities
	void startAllTasks();
	
	/// start all associated timers
	void startAllTimers();
	
	Smart::StatusCode connectPoseServiceIn(const std::string &serverName, const std::string &serviceName);

	// return singleton instance
	static ComponentRobotSimulator* instance()
	{
		if(_componentRobotSimulator == 0) {
			_componentRobotSimulator = new ComponentRobotSimulator();
		}
		return _componentRobotSimulator;
	}
	
	static void deleteInstance() {
		if(_componentRobotSimulator != 0) {
			delete _componentRobotSimulator;
		}
	}
	
	// connections parameter
	struct connections_struct
	{
		// component struct
		struct component_struct
		{
			// the name of the component
			std::string name;
			std::string initialComponentMode;
			std::string defaultScheduler;
			bool useLogger;
		} component;
		
		//--- task parameter ---
		struct Activity1_struct {
			double minActFreq;
			double maxActFreq;
			std::string trigger;
			// only one of the following two params is 
			// actually used at run-time according 
			// to the system config model
			double periodicActFreq;
			// or
			std::string inPortRef;
			int prescale;
			// scheduling parameters
			std::string scheduler;
			int priority;
			int cpuAffinity;
		} activity1;
		
		//--- upcall parameter ---
		
		//--- server port parameter ---
		struct LaserServiceOut_struct {
				std::string serviceName;
				std::string roboticMiddleware;
		} laserServiceOut;
		struct MapServiceIn_struct {
				std::string serviceName;
				std::string roboticMiddleware;
		} mapServiceIn;
		struct NavigationVelocityServiceIn_struct {
				std::string serviceName;
				std::string roboticMiddleware;
		} navigationVelocityServiceIn;
		struct PoseServiceOut_struct {
				std::string serviceName;
				std::string roboticMiddleware;
		} poseServiceOut;
	
		//--- client port parameter ---
		struct PoseServiceIn_struct {
			std::string serverName;
			std::string serviceName;
			std::string wiringName;
			long interval;
			std::string roboticMiddleware;
		} poseServiceIn;
		
		// -- parameters for PlainOpcUaComponentRobotSimulatorExtension
		
	} connections;
};
#endif
